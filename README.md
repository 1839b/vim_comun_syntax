`0 10 "comun rocks" -->`

# Comun Syntax For Vim

Move the contents of the `vim_config` directory to one your **vim's config folder**!

- **vim** `~/.vim `
- **neovim** `~/.config/nvim/`

## How It Works

`ftdetect/cmn.vim` flags files ending with `*.cmn` as the filetype "comun" meaning `syntax/comun.vim` is ran.

## Problems You Might Face

### I Don't Want Two Spaces!

Don't include `ftplugin/cmn.vim`!

### Indentation Not Consistent On Newlines

Try `set smartindent`.

### I Need Kebab Case/Snake Case

Replace CmnFun as described by the comment in `syntax/comun.vim`!

## Credits

[drummyfish](http://www.tastyfish.cz) for the syntax commands used in the vim_config love heart x

https://git.coom.tech/drummyfish/comun/raw/branch/master/other/vim_syntax_highlight.txt