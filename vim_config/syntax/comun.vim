syntax match CmnCom /#[^#\n]*[#\n]/
syntax match CmnStr /"[^"]*"/
syntax match CmnFun /\(\a\|_\)\w*:/
" kebab-case/snake_case (replace above)
" syntax match CmnFun /\(\a\|_\|-\)\+\w*:/
syntax match CmnDir /\~\S\+/
syntax match CmnPtr /\$\S*/
syntax match CmnCtr /\(!\?@\+'\?\|\(\n\| \)?\( \|\n\|'\)\|;\|!\?\.\)/
syntax match CmnPre /\[[^\[\]]*\]/

highlight CmnCom ctermfg=gray
highlight CmnStr ctermfg=green
highlight CmnFun ctermfg=yellow
highlight CmnDir ctermfg=magenta
highlight CmnPtr ctermfg=brown
highlight CmnCtr ctermfg=red
highlight CmnPre ctermfg=blue